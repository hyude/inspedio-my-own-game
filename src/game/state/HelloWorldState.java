package game.state;

import com.inspedio.entity.InsState;
import com.inspedio.entity.basic.InsText;

public class HelloWorldState extends InsState{

	public void create() {
		InsText hello = new InsText("Hello World!");
		this.add(hello);
	}

}
