package game.object;

import com.inspedio.entity.ui.InsButton;

public class BlinkingButton extends InsButton{

	int timer;
	int period;
	
	public BlinkingButton(int X, int Y, int Width, int Height, String Caption, int Color, int Period) {
		super(X, Y, Width, Height, Caption, Color);
		this.period = Period;
		this.timer = period;
	}

	public void blink(){
		if(timer == 0){
			visible = !visible;
			timer = period;
		}
		timer--;
	}
	
	public void update(){
		super.update();
		blink();
	}
}
