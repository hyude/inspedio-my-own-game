package game.object;

import com.inspedio.entity.InsSprite;
import com.inspedio.entity.sprite.InsAnimatedSprite;

public class MultiSprite extends InsAnimatedSprite{

	InsSprite hair;
	InsSprite cloth;
	InsSprite pants;
	
	
	public MultiSprite(int X, int Y, int Width, int Height) {
		super("/game/body.png", X, Y, Width, Height);
		
		this.hair = new InsSprite("/game/hair.png", X, Y, Width, Height);
		this.cloth = new InsSprite("/game/cloth.png", X, Y, Width, Height);
		this.pants = new InsSprite("/game/pants.png", X, Y, Width, Height);
	}
	
	public void setPosition(int X, int Y){
		super.setPosition(X, Y);
		this.hair.setPosition(X, Y);
		this.cloth.setPosition(X, Y);
		this.pants.setPosition(X, Y);
	}
	
	public void setFrame(int Frame){
	}

}
