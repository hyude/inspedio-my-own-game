package game;

import game.state.HelloWorldState;

import com.inspedio.enums.ScreenOrientation;
import com.inspedio.system.InsMain;

public class MyLauncher extends InsMain{

	protected void init() {
		this.init(new HelloWorldState(), ScreenOrientation.LANDSCAPE);
	}

}
